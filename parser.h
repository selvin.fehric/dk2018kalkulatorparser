#ifndef PARSER_H_
#define PARSER_H_ 
#include <vector>
#include "token.h"
struct Parser {
  int currentIndex;
  std::vector<Token> tokens;

  Parser(std::vector<Token> const &);
  bool parse();
  bool S();
  bool S1();
  bool Sp();
  bool E();
  bool Ep();
  bool Ep1();
  bool Ep2();
  bool T();
  bool T1();
  bool T2();
  bool T3();
  bool Tp();
  bool Tp1();
  bool Tp2();
  bool terminal(int t);
  bool epsilon();

};

#endif /* ifndef PARSER_H_ */
