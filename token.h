// <token.h>
#ifndef _TOKEN_ 
#define _TOKEN_ 
#include <string>

const int ID = 10;
const int IF = 2;
const int FOR = 3;
const int THEN = 4;
const int ASSIGN = 5;
const int RELOP = 6;	
const int NUMBER = 8;
const int WS = 9;
const int TO = 12;
const int OPENPAR = 20;
const int CLOSEPAR = 21;
const int PLUS = 22;
const int MULT = 23;

class Token { 
  public: 
    int tag; 
    std::string lexeme; 

    Token(int i=0, std::string ="") ;
    Token& operator=(Token const & t);
    void print() const;
    std::string toString() const;
    static std::string getTokenName(int );
}; 

std::ostream& operator<<(std::ostream&, Token const &);

#endif
