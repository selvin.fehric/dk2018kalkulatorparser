%option header-file="lex.yy.h"
%{
#include "token.h"
extern "C" int yywrap();
%}

ws            [ \t\n]
letter        [a-zA-Z]
digit         [0-9]
number        {digit}+
identifier    {letter}({letter}|{digit})*

%%

{number}  { return NUMBER; } 
{identifier}    { return ID;}
[*+();]    { return yytext[0];}
%{
/* \(        { return OPENPAR; }  */
/* \)        { return CLOSEPAR; }  */
/* \+        { return PLUS; }  */
/* \*        { return MULT; }  */
%}
{ws}       ;
.          {printf("nepoznat karakter '%c'\n", yytext[0]); 
            return 0;}

%%

int yywrap(){
  return 1;
}
