#include <iostream>
#include <vector>

#include "token.h"
#include "lex.yy.h"
#include "parser.h"

using namespace std;
int main(void)
{
  vector<Token> tokens;
  int klasa_tokena;

  while( (klasa_tokena = yylex()) )
    tokens.push_back(Token(klasa_tokena, yytext));

  Parser p(tokens);
  if(p.parse())
    cout << "Ispravna sintaksa." << endl;
  else
    cout << "Neispravna sintaksa." << endl;

  // for(auto t : tokens)
  //   cout << t << " ";
  // cout << endl;

  return 0;
}
