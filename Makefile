parser: lex.yy.c 
	g++ -std=c++11 main.cpp parser.cpp token.cpp lex.yy.c -o parser
lex.yy.c:
	flex scanner.l
clean:
	rm lex.yy.c lex.yy.h parser
