#include <iostream>
#include "parser.h"

Parser::Parser(std::vector<Token> const & t)
  : tokens(t), currentIndex(0) {} 

bool Parser::parse(){
  bool s = S() && currentIndex == tokens.size();
  // std::cout<< "size: " << tokens.size() 
  //   << ", currentIndex: " << currentIndex << std::endl;
  return s;
}

bool Parser::terminal(int t) { 
  if(currentIndex == tokens.size())
    return false;
  return tokens.at(currentIndex++).tag == t ; 
}

bool Parser::epsilon(){
  return true;
}
bool Parser::S(){
  int save = currentIndex;
  return
    S1()
    || ( (currentIndex = save), terminal(';') )
    ;
}

bool Parser::S1(){
  return E() 
    && terminal(';') 
    && Sp();
}

bool Parser::Sp(){
  int save = currentIndex;
  return
    S()
    || ((currentIndex = save), epsilon())
    ;
}

bool Parser::E(){
  return T() && Ep();
}

bool Parser::Ep(){
  int save = currentIndex;
  return
    Ep1()
    || ((currentIndex = save), Ep2())
    ;
}

bool Parser::Ep1(){
  return terminal('+')
    && T()
    && Ep();
}

bool Parser::Ep2(){
  return epsilon();
}

bool Parser::T(){
  int save = currentIndex;
  return
    T1()
    || ((currentIndex = save), T2())
    || ((currentIndex = save), T3())
    ;
}

bool Parser::T1(){
  return terminal(NUMBER)
    && Tp();
}

bool Parser::T2(){
  return terminal('(')
    && E()
    && terminal(')')
    ;
}

bool Parser::T3(){
  return terminal(ID);
}

bool Parser::Tp(){
  int save = currentIndex;
  return
    Tp1()
    || ((currentIndex = save), Tp2())
    ;
}

bool Parser::Tp1(){
  return terminal('*') && T();
}

bool Parser::Tp2(){
  return epsilon();
}

