#include <iostream> 
#include <string> 
#include "token.h"

Token::Token( int i           /* =0   */
              , std::string s   /* =""  */ 
              ) 
  : tag(i), lexeme(s)
{} 

Token& Token::operator=(Token const & t) { 
  tag = t.tag; 
  lexeme = t.lexeme; 
  return *this; 
} 

std::string Token::toString() const { 
  return "<" + getTokenName(tag) + ", '" + lexeme + "'>"; 
} 

void Token::print() const { 
  std::cout << toString() << std::endl ; 
} 

std::string Token::getTokenName(int tag){
  switch(tag){
    case IF:
      return "IF";
    case ID:
      return "ID";
    case FOR:
      return "FOR";
    case THEN:
      return "THEN";
    case TO:
      return "TO";
    case ASSIGN:
      return "ASSIGN";
    case WS:
      return "WS";
    case RELOP:
      return "RELOP";
    case NUMBER:
      return "NUMBER";
    case 0:
      return "unknown";
    default:
      return std::string(1, tag)  ;
  }
}

std::ostream& operator<<(std::ostream& out, Token const & t){
  out << t.toString();
  return out;
}









